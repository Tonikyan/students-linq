﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mic.Homework.LINQ.Students
{
    class MainClass
    { 
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            var students = CreateStudents(50);
            Print(students);
            var res1 = new List<List<Student>>(20);
            for (int i = 1; i <= res1.Capacity; i++)
            {
                var res = students.Where(p => p.Mark == i).ToList();
                res1.Add(res);
            }
            Print(res1);

            var res2 = students.OrderBy(p => p.University).ToList();

            Dictionary<Universities, List<Student>> res3 = students
                .OrderBy(p => p.Mark)
                .GroupBy(p => p.University)
                .ToDictionary(p => p.Key, p => p.ToList());
            Print(res3);
                
            Console.ReadLine();
        }

        static List<Student> CreateStudents(int count)
        {
            Random rd = new Random();
            string[] names = { "Ալեքս ", "Լիլիթ  ", "Դավիթ ", "Արման  ", "Լեվոն ","Մարտին" };
            string[] surnanmes = { "Մելքոնյան", "Մանուկյան", "Սարգսյան", "Գրիգորյան", "Գեվորգյան", "Հովանիսյան" };
            List<Student> list = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                Student st = new Student()
                {
                    Name = names[rd.Next(names.Length)],
                    Surname = surnanmes[rd.Next(surnanmes.Length)],
                    Mark =(byte) rd.Next(1, 21),
                    Age =(byte) rd.Next(7,20),
                    University = (Universities)rd.Next(3)
                };
                list.Add(st);
            }
            return list;
        }

        static void Print(Dictionary<Universities, List<Student>> list)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            //Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine("\tName\t\tSurname\t\t\tAge\t\tMark");
            foreach (var grup in list.Values)
            {
                var i = 0;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(grup[i++].University);
                Console.ForegroundColor = ConsoleColor.Green;
                Print(grup);
                //foreach (var item in grup)
                //{
                //    Console.WriteLine($"\t{item.FullName}\t|\t{item.Age}\t|\t{item.Mark}");
                //}
            }
            Console.ResetColor();
        }

        static void Print(List<Student> list)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\tName\t\tSurname\t\t\tAge\t\tMark\t\tUniversity");
            Console.ForegroundColor = ConsoleColor.Green;
            foreach (var item in list)
            {
                Console.WriteLine($"\t{item.FullName}\t|\t{item.Age}\t|\t{item.Mark}\t|\t{item.University}");
            }
            Console.ResetColor();
        }

        static void Print(List<List<Student>> list)
        {
            int i = 0;
            foreach (var item in list)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Mark = {++i}");
                Print(item);
            }
        }
    }
}
