﻿using System;

namespace Mic.Homework.LINQ.Students
{
    class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FullName => $"{Name}\t|\t{Surname}";
        public byte Mark { get; set; }
        public byte Age { get; set; }
        public Universities University;
        public override string ToString()
        {
            return Mark.ToString();
        }
    }

    enum Universities
    {
        YSU,
        EDU,
        AUA,    
    }
}
